import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
 
/**
 *
 * @author sqlitetutorial.net
 */
public class Database {
	private static String url = "jdbc:sqlite:database.db";
	ArrayList<String> authors;
	ArrayList<Integer> authorsIDs;
	ArrayList<String> albums;
	ArrayList<String> songs;
	ArrayList<String> orders;
	ArrayList<String> translations;
	String lang = "pl";
	Album selectedAlbum;
	utwory selectedSong;
     /**
     * Connect to a sample database
     */
    private Connection connect() {
        // SQLite connection string
        
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }
    /**
     * @param args the command line arguments
     */
      
    public static void createNewAlbumTable() {
        
        
        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS albums (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	title text UNIQUE,\n"
                + "	year integer\n"
                + ");";
        
        try (Connection conn = DriverManager.getConnection(url);
                Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public static void createTranslationsTable() {
        
        
        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS translations (\n"
                + "	id integer,\n"
                + "	language text,\n"
                + "	text text\n"
                + ");";
        
        try (Connection conn = DriverManager.getConnection(url);
                Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public static void createLangTable() {
        
        
        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS language (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	language text\n"
                + ");";
        
        try (Connection conn = DriverManager.getConnection(url);
                Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public static void createOrdersTable() {
        
        
        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS orders (\n"
                + "	album_id integer,\n"
                + "	user_id integer\n"
                + ");";
        
        try (Connection conn = DriverManager.getConnection(url);
                Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public static void createNewAuthorTable() {
        // SQLite connection string
        
        
        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS authors (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	name text\n"
                + ");";
        
        try (Connection conn = DriverManager.getConnection(url);
                Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public static void createNewSongTable() {
        // SQLite connection string
        
        
        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS songs (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	name text,\n"
                + "	album_id integer,\n"
                + "	FOREIGN KEY (album_id) REFERENCES albums(id) ON DELETE CASCADE ON UPDATE CASCADE\n"
                + ");";
        
        try (Connection conn = DriverManager.getConnection(url);
                Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public static void createOwnershipTable() {
        
        
        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS ownerships (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	album_id integer,\n"
                + "	author_id integer,\n"
                + "	FOREIGN KEY (album_id) REFERENCES albums(id) ON DELETE CASCADE ON UPDATE CASCADE,\n"
                + "	FOREIGN KEY (author_id) REFERENCES authors(id) ON DELETE CASCADE ON UPDATE CASCADE\n"
                
                + ");";
        
        try (Connection conn = DriverManager.getConnection(url);
                Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public static void createUsersTable() {
        
        
        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS users (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	name text UNIQUE,\n"
                + "	password text\n"
                + ");";
        
        try (Connection conn = DriverManager.getConnection(url);
                Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void addNewUser(String name, String password) {
        String sql = "INSERT INTO users(name, password) VALUES(?,?)";
 
        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, name);
            pstmt.setString(2, password);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void setSelectedLanguage(String language) {
        String sql = "REPLACE INTO language(id, language) VALUES(1,?)";
 
        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, language);
            
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void addNewOrder(String user, String album) {
        String sql = "INSERT into orders(album_id, user_id) values((select albums.id from albums where albums.title = ?),(select users.id from users where users.name = ?))";
 
        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, album);
            pstmt.setString(2, user);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void selectOrdersByUser(String user){
    	orders = new ArrayList<String>();
    	String sql = "select orders.user_id, albums.title from orders left join albums on albums.id = orders.album_id where title!=\"\" AND orders.user_id = (select users.id from users where users.name = ?)";
    	 System.out.println(sql);
    	try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)){
            pstmt.setString(1, user);
            
           
            ResultSet rs = pstmt.executeQuery();
            selectedAlbum = new Album("", 1, "");
            while (rs.next()) 
            {
            	orders.add(rs.getString("title"));
            	 System.out.println(rs.getString("title"));
            	selectedAlbum.name = rs.getString("title");
            	
            	
            }
    	}
         catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
    }
    
    public void selectTranslationsByLanguage(String language){
    	
    	lang = language;
    	
    	String sql = "select * from translations WHERE language = ? ORDER BY id asc";
    	 
    	try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)){
            pstmt.setString(1, language);
            
           
            ResultSet rs = pstmt.executeQuery(); 
            translations = new ArrayList<String>();
            while (rs.next()) 
            {
            	String x = rs.getString("text");
            	if(x == null) 
            	{
            		x = "";
            	}
            	translations.add(x);
            	System.out.println(rs.getString("text"));
            	
            }
    	}
         catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
    }
    
    public void getSelectedLanguage(){
    	
    	
    	
    	String sql = "select * from language WHERE id = 1";
    	 
    	try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)){
            
            
           
            ResultSet rs = pstmt.executeQuery(); 
            
            while (rs.next()) 
            {
            	lang = rs.getString("language");
            	System.out.println(lang);
            	
            }
    	}
         catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
    }
    
    public boolean checkUserPassword(String name, String password){
    	
    	boolean found = false;
    	
    	String sql = "SELECT count (*) > 0 , name FROM users WHERE name = ? AND password = ? LIMIT 1";
    	 
    	try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)){
            pstmt.setString(1, name);
            pstmt.setString(2, password);
           
            ResultSet rs = pstmt.executeQuery();
            
            
            
            while (rs.next()) 
            {
            	 found = rs.getBoolean(1);
          
            }
            
            
    	}
         catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    	
    	return found;
        
    }
    
    public void addNewAuthor(String name) {
        String sql = "INSERT INTO authors(name) VALUES(?)";
 
        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, name);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void updateAuthor(String name, String new_name) {
        String sql = "UPDATE authors SET name= ? WHERE name = ?;";
 
        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, new_name);
            pstmt.setString(2, name);
            
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void deleteAuthor(String name) {
        String sql = "DELETE FROM authors WHERE name = ?";
 
        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, name);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void selectAllAuthors(){
        String sql = "SELECT id, name FROM authors";
        authors = new ArrayList<String>();
        authorsIDs = new ArrayList<Integer>();
        
        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
        	
        	
            // loop through the result set
            while (rs.next()) {
            	
            	//data += rs.getInt("year"), rs.getString("title"), rs.getString("authors");
            	
            	//albumFrame.model.addRow(new Object[]{rs.getInt("id"), rs.getInt("year"), rs.getString("title"), rs.getString("authors")});
            	
            	//data 
            	//albumFrame.table.inse(t);
            	
            	authors.add(rs.getString("name"));
            	authorsIDs.add(rs.getInt("id"));
            
                System.out.println(rs.getInt("id") +  "\t" +
                				   rs.getString("name"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
        //return authors;
    }
    
    public void selectAllSongs(){
        String sql = "SELECT songs.id, name, title FROM songs inner join albums on albums.id = songs.album_id";
        songs = new ArrayList<String>();
        
        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
        	
        	
            // loop through the result set
            while (rs.next()) {
            	
            	//data += rs.getInt("year"), rs.getString("title"), rs.getString("authors");
            	
            	//albumFrame.model.addRow(new Object[]{rs.getInt("id"), rs.getInt("year"), rs.getString("title"), rs.getString("authors")});
            	
            	//data 
            	//albumFrame.table.inse(t);
            	
            	songs.add(rs.getString("name"));
            
                System.out.println(rs.getInt("id") +  "\t" +
                				   rs.getString("name"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
        //return authors;
    }
    
    
    public void addNewAlbum(String title, int year, int[] authors) {
        String sql = "REPLACE INTO albums(title, year) VALUES(?,?)";
 
        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            pstmt.setString(1, title);
            pstmt.setInt(2, year);
           
            pstmt.executeUpdate();
            int album_id = pstmt.getGeneratedKeys().getInt(1);
            System.out.println("album: " + album_id);
            
            deleteOwnership(album_id);
            for(int i = 0 ; i < authors.length; i++) 
            {
            	addNewOwnership(album_id, authorsIDs.get(authors[i]));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void deleteOwnership(int id) {
        String sql = "DELETE FROM ownerships WHERE album_id = ?";
 
        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, id);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void addNewOwnership(int album, int author) {
        String sql = "INSERT INTO ownerships(album_id, author_id) VALUES(?,?)";
 
        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, album);
            pstmt.setInt(2, author);
           
            pstmt.executeUpdate();
            
           
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void updateAlbum(String title, int year, int[] authors, String old_title) {
    	//addNewAlbum(title, year, authors);
    	//if(!old_title.equals(title)) deleteAlbum(old_title);
        String sql = "UPDATE albums SET title= ?, year = ? WHERE title = ?;";
 
        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, title);
            pstmt.setInt(2, year);
            pstmt.setString(3, old_title);
            
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void deleteAlbum(String name) {
        String sql = "DELETE FROM albums WHERE title = ?";
 
        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, name);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void selectAllAlbums(){
        String sql = "SELECT albums.id, title, year, group_concat(author_id), group_concat(authors.name) as authors FROM albums LEFT JOIN ownerships ON albums.id = ownerships.album_id INNER JOIN authors ON ownerships.author_id = authors.id GROUP BY albums.id";
        
        albums = new ArrayList<String>();
        
        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
        	
        	Object[][] data = {
    			    {"Kathy", "Smith", "Snowboarding"},
    			    {"John", "Doe", "Rowing"},
    			    {"Sue", "Black", "Knitting"},
    			    {"Jane", "White", "Speed reading"},
    			    {"Joe", "Brown", "Pool"}
    			};
          //  
        	
            // loop through the result set
            while (rs.next()) {
            	
            	//data += rs.getInt("year"), rs.getString("title"), rs.getString("authors");
            	
            	try{
            		albumFrame.model.addRow(new Object[]{rs.getInt("id"), rs.getInt("year"), rs.getString("title"), rs.getString("authors")});
            	}
            	catch(Exception e)
            	{}
            	
            	//data 
            	//albumFrame.table.inse(t);
            	
            	albums.add(rs.getString("title"));
            
                System.out.println(rs.getInt("id") +  "\t" +
                				   rs.getInt("year") +  "\t" + 
                                   rs.getString("title") + "\t" +
                                   rs.getString("authors"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void selectAlbumByName(String name){
    	
    	String sql = "SELECT albums.id, title, year, group_concat(author_id), group_concat(authors.name) as authors FROM albums LEFT JOIN ownerships ON albums.id = ownerships.album_id INNER JOIN authors ON ownerships.author_id = authors.id WHERE title = ?";
    	 
    	try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)){
            pstmt.setString(1, name);
            
           
            ResultSet rs = pstmt.executeQuery();
            selectedAlbum = new Album("", 1, "");
            while (rs.next()) 
            {
            	selectedAlbum.name = rs.getString("title");
            	selectedAlbum.year = rs.getInt("year");
            	selectedAlbum.artists = rs.getString("authors");
            }
    	}
         catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
    }
    
    public void selectSongByName(String name){
    	
    	String sql = "SELECT songs.id, name, album_id, albums.title FROM songs INNER JOIN albums ON albums.id = songs.album_id WHERE name = ?";
    	 
    	try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)){
            pstmt.setString(1, name);
            
           
            ResultSet rs = pstmt.executeQuery();
            selectedSong = new utwory();
            while (rs.next()) 
            {
            	selectedSong.setNazwaUtworu(rs.getString("name"));
            	selectedSong.setNazwaAlbumu(rs.getString("title"));
          
            }
    	}
         catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
    }
    
    public void selectSongsBySearch(String pattern){
    	
    	String sql = "select songs.name, albums.title, albums.year, ownerships.author_id, authors.name AS aname from songs inner join albums ON albums.id = songs.album_id  left join ownerships on ownerships.album_id = songs.album_id left join authors on authors.id = ownerships.author_id WHERE songs.name LIKE ? OR albums.title LIKE ? OR albums.year LIKE ? OR authors.name LIKE ? ";
    	 
    	try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)){
    		pattern = "%" + pattern + "%";
    		System.out.println(pattern);
            pstmt.setString(1, pattern);
            pstmt.setString(2, pattern);
            pstmt.setString(3, pattern);
            pstmt.setString(4, pattern);
            
           
            ResultSet rs = pstmt.executeQuery();
            
            while (rs.next()) 
            {
            	try{
            		System.out.println(rs.getString("name") + rs.getString("title")+ rs.getInt("year")+ rs.getString("aname"));
            		Search.model.addRow(new Object[]{rs.getString("name"), rs.getString("title"), rs.getInt("year"), rs.getString("aname")});
            	}
            	catch(Exception e)
            	{
            		System.out.println(e.toString());
            	}
            	
          
            }
    	}
         catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
    }
    
    public void addNewSong(String title, String album_title) {
        String sql = "INSERT INTO songs(name, album_id) VALUES(?, (select albums.id from albums where albums.title = ?))";
 
        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, title);
            pstmt.setString(2, album_title);
            
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void updateSong(String title, String album, String old_title) {
        String sql = "UPDATE songs SET name= ?, album_id = (SELECT albums.id from albums where albums.title = ?) WHERE name = ?";
 
        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, title);
            pstmt.setString(2, album);
            pstmt.setString(3, old_title);
            
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void deleteSong(String title) {
        String sql = "DELETE FROM songs WHERE name = ?";
 
        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, title);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    
    public static void main(String[] args) {
        
       /*
       createNewAlbumTable();
       createNewAuthorTable();
       createNewSongTable();
       createOwnershipTable();
       */
    	
        Database db = new Database();
        //db.addNewAuthor("testowyautor");
        //db.addNewAlbum("Album", 1999, "fdeffe");
        //db.addNewSong("title", "album", "uthors");
        //db.selectAllAlbums();
        //db.selectAllAuthors();
        //db.selectAlbumByName("rrr");
        //db.selectAllSongs();
        //db.createUsersTable();
        //db.addNewUser("alek", "alek1");
        boolean x = db.checkUserPassword("alek", "alek1");
        System.out.println(x);
        x = db.checkUserPassword("alek", "alek2");
        System.out.println(x);
        
        db.createOrdersTable();
        db.selectTranslationsByLanguage("en");
    }
}