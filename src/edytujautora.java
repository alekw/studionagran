

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.apache.commons.lang3.StringUtils;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class edytujautora {

    private JFrame frame;
    private JTextField nazwisko;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    edytujautora window = new edytujautora();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public edytujautora() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
    	
    	Database db = new Database();
		db.getSelectedLanguage();
    	
	    if(db.lang == null) 
	    {
	    	db.lang = "pl";
	    }
	    System.out.println("Obecny jezyk to: " + db.lang);
		db.selectTranslationsByLanguage(db.lang);
    	
        frame = new JFrame();
        frame.setBounds(100, 100, 730, 425);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        

        
        JLabel lblUtworyIAlbumy = new JLabel(db.translations.get(11));
        lblUtworyIAlbumy.setBounds(83, 44, 77, 29);
        frame.getContentPane().add(lblUtworyIAlbumy);
        
        nazwisko = new JTextField();
        nazwisko.setBounds(187, 150, 147, 20);
        frame.getContentPane().add(nazwisko);
        nazwisko.setColumns(10);
        
		db.selectAllAuthors();
		
		JScrollPane scrollableList = new JScrollPane();
		scrollableList.setLocation(212, 40);
		scrollableList.setSize(100, 100);
		
		frame.getContentPane().add(scrollableList);
		
		
		
		
		JList list2;
		list2 = new JList(db.authors.toArray());
		scrollableList.setViewportView(list2);
		list2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String autor = list2.getSelectedValue().toString();
				nazwisko.setText(autor);
			}
		});
		list2.setBounds(187, 11, 147, 92);
		//frame.getContentPane().add(list2);
		//frame.getContentPane().add(list);
		list2.setVisibleRowCount(1);
		list2.setBackground(Color.WHITE);
		
		   JButton edytuj = new JButton(db.translations.get(14));
	        edytuj.addMouseListener(new MouseAdapter() {
	        	@Override
	        	public void mouseClicked(MouseEvent e) {
	        		String name= list2.getSelectedValue().toString();
	        		String new_name=nazwisko.getText();
	        		if(!StringUtils.isBlank(new_name)) {
						
		        		db.updateAuthor(name, new_name);
		        		
		        		frame.dispose();
		        		edytujautora window = new edytujautora();
	                    window.frame.setVisible(true);
					}		

	        		
	        	}
	        });
	        
	        edytuj.setBounds(187, 181, 147, 23);
	        frame.getContentPane().add(edytuj);
	        
	        JLabel lblNewLabel = new JLabel(db.translations.get(13));
	        lblNewLabel.setBounds(83, 153, 94, 14);
	        frame.getContentPane().add(lblNewLabel);
    }
}

