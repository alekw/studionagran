import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;

import org.apache.commons.lang3.StringUtils;

public class albumFrame {

	public static JTable table;
	public static DefaultTableModel model;
	private JFrame frame;
	private JTextField textField;
	private JPanel contentPane;
	private JTextField nameTextfield;
	private JTextField yearTextfield;
	private static JList list;
	public Database baza;
	//Database db;
	public static ArrayList<String> authors = null;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					albumFrame window = new albumFrame();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public albumFrame() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		Database db = new Database();
		db.getSelectedLanguage();
    	
	    if(db.lang == null) 
	    {
	    	db.lang = "pl";
	    }
	    System.out.println("Obecny jezyk to: " + db.lang);
		db.selectTranslationsByLanguage(db.lang);
		
		//db.addNewAlbum();
		
		frame = new JFrame();
		frame.setBounds(0, 0, 800, 600);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		

		

		
		Label label = new Label(db.translations.get(15));
		label.setBounds(174, 10, 62, 22);
		frame.getContentPane().add(label);
		
		nameTextfield = new JTextField();
		nameTextfield.setBounds(20, 59, 86, 20);
		frame.getContentPane().add(nameTextfield);
		nameTextfield.setColumns(10);
		
		Label label_1 = new Label(db.translations.get(16));
		label_1.setBounds(27, 31, 62, 22);
		frame.getContentPane().add(label_1);
		
		yearTextfield = new JTextField();
		yearTextfield.setBounds(116, 59, 86, 20);
		frame.getContentPane().add(yearTextfield);
		yearTextfield.setColumns(10);
		
		Label label_2 = new Label(db.translations.get(17));
		label_2.setBounds(128, 31, 62, 22);
		frame.getContentPane().add(label_2);
		
		db.selectAllAuthors();


		JScrollPane scrollableList = new JScrollPane();
		scrollableList.setLocation(212, 59);
		scrollableList.setSize(100, 100);
		
		frame.getContentPane().add(scrollableList);
		
		list = new JList(db.authors.toArray());
		scrollableList.setViewportView(list);
		//frame.getContentPane().add(list);
		list.setVisibleRowCount(1);
		list.setBackground(Color.WHITE);
		
		Label label_3 = new Label(db.translations.get(11));
		label_3.setBounds(225, 31, 62, 22);
		frame.getContentPane().add(label_3);
		
		JButton btnDodajAlbum = new JButton(db.translations.get(18));
		JComboBox albumsComboBox = new JComboBox();
		//albumsComboBox.setSelectedIndex(-1);
		
		btnDodajAlbum.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				String name = nameTextfield.getText();
				if(!StringUtils.isBlank(name)) {
					int year = Integer.parseInt(yearTextfield.getText());
					int[] authors = list.getSelectedIndices();
					for(int i = 0; i < authors.length; i++) 
					{
						//authors[i]++;
						System.out.println(name + " " + year + " " + authors[i]);
						//db.authorsIDs.get(i)
					}
					String oldTitle = "";
					if(albumsComboBox.getSelectedIndex() != -1) 
					{
						oldTitle = albumsComboBox.getSelectedItem().toString();
					}
					if(oldTitle.isEmpty() || oldTitle.equals(name)) 
					{
						db.addNewAlbum(name, year, authors);
					}
					else 
					{
						db.updateAlbum(name, year, authors, oldTitle);
					}
					
					
					
					//Album added = new Album(name, year, "");
					
					frame.setVisible(false);
					albumFrame.main(null);
				}		
				
			}
		});
		btnDodajAlbum.setBounds(323, 58, 113, 23);
		
		frame.getContentPane().add(btnDodajAlbum);
		//String week[]= { "Monday","Tuesday","Wednesday", 
               // "Thursday","Friday","Saturday","Sunday"};
		
		db.selectAllAlbums();
		
		
		albumsComboBox.setBounds(20, 119, 130, 20);
		
		for(int i = 0; i< db.albums.size();i++) 
		{
			albumsComboBox.addItem(db.albums.get(i));
		}
		
		frame.getContentPane().add(albumsComboBox);
		albumsComboBox.setSelectedIndex(-1);
		
		
		albumsComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String name = albumsComboBox.getSelectedItem().toString();
				//nameTextfield.setText(albumsComboBox.getSelectedItem().toString());
				db.selectAlbumByName(name);
				System.out.println(db.selectedAlbum.name);
				nameTextfield.setText(db.selectedAlbum.name);
				yearTextfield.setText(Integer.toString(db.selectedAlbum.year));
				
				String authors = db.selectedAlbum.artists;
				List<String> authorsList = Arrays.asList(authors.split(","));
				
				int[] selected = new int[100];
				Arrays.fill(selected, -1);
				
				for(int i = 0; i < authorsList.size(); i++) 
				{
					list.setSelectedValue(authorsList.get(i), true);
					selected[i] = list.getSelectedIndex();
					System.out.println(authorsList.get(i));
					
				}
				
				
				
				list.setSelectedIndices(selected);
				
				//list.setSelectedValue("EEE", true);
				
			}
		});
		

		
		
		
		
		
		
		
		
		
		
		model = new DefaultTableModel(); 
		

		// Create a couple of columns
		model.addColumn(db.translations.get(21));
		model.addColumn(db.translations.get(17));
		model.addColumn(db.translations.get(16)); 
		model.addColumn(db.translations.get(11));

		// Append a row 
		//model.addRow(new Object[]{"1", "2", "3"});
		
		table = new JTable(model);
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(20,  235,  658,  254);
		//table.setFillsViewportHeight(true);
		
		db.selectAllAlbums();
		frame.getContentPane().add(scrollPane);
		
		Label label_4 = new Label(db.translations.get(19));
		label_4.setBounds(20, 91, 86, 22);
		frame.getContentPane().add(label_4);
		
		JButton btnKasujAlbum = new JButton(db.translations.get(20));
		btnKasujAlbum.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String name = albumsComboBox.getSelectedItem().toString();
				db.deleteAlbum(name);
				frame.setVisible(false);
				albumFrame.main(null);
				
			}
		});
		btnKasujAlbum.setBounds(322, 118, 114, 23);
		frame.getContentPane().add(btnKasujAlbum);
		
	}
}
