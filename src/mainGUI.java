import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.JComboBox;

public class mainGUI {
	public static DefaultTableModel model;
	private JFrame frame;
	private JTextField textField;
	private JPanel contentPane;
	Database db;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					mainGUI window = new mainGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public mainGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		
		
	    db = new Database();
		
		db.createNewAlbumTable();
		db.createNewAuthorTable();
		db.createNewSongTable();
		db.createOwnershipTable();
		db.createUsersTable();
		db.createOrdersTable();
		db.createLangTable();
		db.createTranslationsTable();
		db.getSelectedLanguage();
	    	
	    if(db.lang == null) 
	    {
	    	db.lang = "pl";
	    }
	    System.out.println("Obecny jezyk to: " + db.lang);
		db.selectTranslationsByLanguage(db.lang);
		
		
		frame = new JFrame();
		frame.setTitle(db.translations.get(0));
		frame.setBounds(0, 0, 800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblStudioNagra = new JLabel(db.translations.get(1));
		lblStudioNagra.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblStudioNagra.setBounds(310, 11, 219, 33);
		frame.getContentPane().add(lblStudioNagra);
		
		JButton btnDodajAutora = new JButton(db.translations.get(2));
		btnDodajAutora.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				//this.setVisible(false);
		        new dodajAutora().main(null);
			}
		});
		btnDodajAutora.setBounds(82, 87, 150, 23);
		frame.getContentPane().add(btnDodajAutora);
		
		JButton btnNewButton = new JButton(db.translations.get(41));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//frame.setVisible(false);
				
		        new albumFrame().main(null);
		       
		        
			}
		});
		btnNewButton.setBounds(82, 141, 150, 23);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnZarzdzajUtworami = new JButton(db.translations.get(5));
		btnZarzdzajUtworami.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new utworOkienko().main(null);
			}
		});
		btnZarzdzajUtworami.setBounds(82, 193, 150, 23);
		frame.getContentPane().add(btnZarzdzajUtworami);
		
		JButton btnNewButton_1 = new JButton(db.translations.get(3));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new usunautora().main(null);
			}
		});
		btnNewButton_1.setBounds(243, 87, 150, 23);
		frame.getContentPane().add(btnNewButton_1);
		
		JLabel label = new JLabel("1.");
		label.setBounds(26, 91, 46, 14);
		frame.getContentPane().add(label);
		
		JLabel label_1 = new JLabel("2.");
		label_1.setBounds(26, 145, 46, 14);
		frame.getContentPane().add(label_1);
		
		JLabel label_2 = new JLabel("3.");
		label_2.setBounds(26, 197, 46, 14);
		frame.getContentPane().add(label_2);
		
		JLabel label_3 = new JLabel("4.");
		label_3.setBounds(26, 237, 46, 14);
		frame.getContentPane().add(label_3);
		
		JButton btnWyszukiwanie = new JButton(db.translations.get(6));
		btnWyszukiwanie.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Search().main(null);
			}
		});
		btnWyszukiwanie.setBounds(82, 233, 150, 23);
		frame.getContentPane().add(btnWyszukiwanie);
		
		JButton btnEdytujAutora = new JButton(db.translations.get(4));
		btnEdytujAutora.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new edytujautora().main(null);
			}
		});
		btnEdytujAutora.setBounds(403, 87, 150, 23);
		frame.getContentPane().add(btnEdytujAutora);
		
		JLabel label_4 = new JLabel("5.");
		label_4.setBounds(26, 275, 46, 14);
		frame.getContentPane().add(label_4);
		
		JButton btnsklep = new JButton(db.translations.get(7));
		btnsklep.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Sklep().main(null);
			}
		});
		btnsklep.setBounds(82, 271, 150, 23);
		frame.getContentPane().add(btnsklep);
		
		JButton btnPanelKlienta = new JButton(db.translations.get(8));
		btnPanelKlienta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new GUI().main(null);
			}
		});
		btnPanelKlienta.setBounds(243, 271, 150, 23);
		frame.getContentPane().add(btnPanelKlienta);
		
		JComboBox langComboBox = new JComboBox();
		langComboBox.addItem("pl");
		langComboBox.addItem("en");
		langComboBox.setSelectedItem(db.lang);
		langComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String langSel = langComboBox.getSelectedItem().toString();
				
				db.setSelectedLanguage(langSel);
				
				
				frame.dispose();
				
				mainGUI.main(null);
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
			}
		});
		langComboBox.setBounds(679, 21, 80, 20);
		frame.getContentPane().add(langComboBox);
		
		
		
	}
}
