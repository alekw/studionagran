import java.awt.EventQueue;
 
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;

import org.apache.commons.lang3.StringUtils;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.JPanel;
 
public class dodajAutora {
 
 
    private JFrame frame;
    private JTextField nazwisko;
 
    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    dodajAutora window = new dodajAutora();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
 
    /**
     * Create the application.
     */
    public dodajAutora() {
        initialize();
    }
 
    /**
     * Initialize the contents of the frame.
     */
    ArrayList<String> autorzy = new ArrayList<String>();
    private void initialize() {
    	Database db = new Database();
		db.getSelectedLanguage();
    	
	    if(db.lang == null) 
	    {
	    	db.lang = "pl";
	    }
	    System.out.println("Obecny jezyk to: " + db.lang);
		db.selectTranslationsByLanguage(db.lang);
        frame = new JFrame();
        frame.setBounds(100, 100, 450, 300);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.getContentPane().setLayout(null);
       
        nazwisko = new JTextField();
        nazwisko.setBounds(201, 34, 86, 20);
        frame.getContentPane().add(nazwisko);
        nazwisko.setColumns(10);
       
        JLabel lblNazwisko = new JLabel(db.translations.get(9));
        lblNazwisko.setBounds(84, 37, 86, 14);
        frame.getContentPane().add(lblNazwisko);
       
        
        
        JLabel komunikat = new JLabel("");
        komunikat.setBounds(114, 83, 277, 14);
        frame.getContentPane().add(komunikat);
       
        JButton Dodajautora = new JButton(db.translations.get(2));
        Dodajautora.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
            	
            	String name = nazwisko.getText();
				if(!StringUtils.isBlank(name)) {
					System.out.print(name);
	                db.addNewAuthor(name);
	                komunikat.setText(db.translations.get(10) + name);
				}	            

            };
        });
        Dodajautora.setBounds(191, 109, 111, 23);
        frame.getContentPane().add(Dodajautora);
 
    }
}