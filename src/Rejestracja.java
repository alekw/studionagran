import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang3.StringUtils;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.BigInteger;
import java.security.MessageDigest;

public class Rejestracja extends JFrame {

	private JPanel contentPane;
	private JTextField imief;
	private JButton przycisk;
	private JPasswordField haslof;
	private JButton btnZatwierdz;
	
	

	public void ustawDaneKlienta(Klient kopia) {
		//przepisanie danych z interfejsu
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Rejestracja frame = new Rejestracja();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Rejestracja() {
		
		Database db = new Database(); 
		db.getSelectedLanguage();
    	
	    if(db.lang == null) 
	    {
	    	db.lang = "pl";
	    }
	    System.out.println("Obecny jezyk to: " + db.lang);
		db.selectTranslationsByLanguage(db.lang);
		
		
		setTitle(db.translations.get(36));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel imie = new JLabel(db.translations.get(38));
		imie.setBounds(10, 11, 80, 14);
		contentPane.add(imie);
		
		imief = new JTextField();
		imief.setBounds(133, 8, 86, 20);
		contentPane.add(imief);
		imief.setColumns(10);
		
		JLabel Haslo = new JLabel(db.translations.get(39));
		Haslo.setBounds(10, 42, 80, 14);
		contentPane.add(Haslo);
		
		haslof = new JPasswordField();
		haslof.setBounds(133, 39, 86, 20);
		contentPane.add(haslof);
		
		btnZatwierdz = new JButton(db.translations.get(40));
		btnZatwierdz.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String name=imief.getText();
				String haslo=haslof.getText();
				String haslosha1 = null;
				String haslosha2 = null;
	      
				if(!StringUtils.isBlank(name) && !StringUtils.isBlank(haslo)) {
					
					try {
						MessageDigest digest = MessageDigest.getInstance("SHA-1");
				        digest.reset();
				        digest.update(haslo.getBytes("utf8"));
				        haslosha1 = String.format("%040x", new BigInteger(1, digest.digest()));
					} catch (Exception e1){
						e1.printStackTrace();
					}
					
					try {
						MessageDigest digest1 = MessageDigest.getInstance("SHA-1");
				        digest1.reset();
				        digest1.update(haslosha1.getBytes("utf8"));
				        haslosha2 = String.format("%040x", new BigInteger(1, digest1.digest()));
					} catch (Exception e1){
						e1.printStackTrace();
					}
				
					
					db.addNewUser(name, haslosha2);
					
					Logowanie.loggedUser = name;
					new Sklep().main(null);
					
					
				}		
				
			}
		});
		btnZatwierdz.setBounds(67, 88, 116, 23);
		contentPane.add(btnZatwierdz);

	}
}
