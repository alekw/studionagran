public class utwory {
	
	
	public String nazwaUtworu;
	public String nazwaAlbumu;
	public String autor;
	
	public utwory() {
		nazwaUtworu="";
		nazwaAlbumu="";
		autor="";
	}
	
	public void setNazwaUtworu(String nowaNazwaUtworu) {
		nazwaUtworu=nowaNazwaUtworu;
	}
	
	public void setNazwaAlbumu(String nowaNazwaAlbumu) {
		nazwaAlbumu=nowaNazwaAlbumu;
	}
	
	public void setAutor(String nowyAutor) {
		autor=nowyAutor;
	}
	
	public String getNazwaUtworu() {
		return nazwaUtworu;
	}
	
	public String getNazwaAlbumu() {
		return nazwaAlbumu;
	}
	
	public String getAutor() {
		return autor;
	}

}