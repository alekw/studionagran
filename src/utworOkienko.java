import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import org.apache.commons.lang3.StringUtils;

import javax.swing.JComboBox;
import javax.swing.JSeparator;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseMotionAdapter;

public class utworOkienko {

	public static List listaUtworow;

	private JFrame frmUtwory;
	private JTextField nazwaUtworu;
	private JTextField nazwaEdytowanegoUtworu;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		//listaUtworow=new ArrayList();
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					utworOkienko window = new utworOkienko();
					window.frmUtwory.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public utworOkienko() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		Database db = new Database();
		db.getSelectedLanguage();
	    	
	    if(db.lang == null) 
	    {
	    	db.lang = "pl";
	    }
	    System.out.println("Obecny jezyk to: " + db.lang);
		db.selectTranslationsByLanguage(db.lang);
		
		frmUtwory = new JFrame();
		frmUtwory.setTitle(db.translations.get(22));
		frmUtwory.setBounds(100, 100, 475, 397);
		frmUtwory.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmUtwory.getContentPane().setLayout(null);
		
		//wybor albumu z bazy
		db.selectAllAlbums();
		
		nazwaEdytowanegoUtworu = new JTextField();
		nazwaEdytowanegoUtworu.setColumns(10);
		nazwaEdytowanegoUtworu.setBounds(240, 231, 101, 20);
		frmUtwory.getContentPane().add(nazwaEdytowanegoUtworu);
		
		db.selectAllSongs();
		
		JComboBox comboListaUtworow = new JComboBox();
		comboListaUtworow.setBounds(292, 190, 110, 20);
		frmUtwory.getContentPane().add(comboListaUtworow);
		
		for(int i = 0 ; i < db.songs.size(); i++) 
		{
			comboListaUtworow.addItem(db.songs.get(i));
		}
		
		JComboBox comboListaAlbumow = new JComboBox();
		comboListaAlbumow.setBounds(240, 69, 86, 20);
		frmUtwory.getContentPane().add(comboListaAlbumow);
		
		JComboBox comboListaEdytowanychAlbumow = new JComboBox();
		comboListaEdytowanychAlbumow.setBounds(240, 262, 101, 20);
		frmUtwory.getContentPane().add(comboListaEdytowanychAlbumow);
		
		for(int i = 0; i< db.albums.size(); i++) 
		{
			comboListaAlbumow.addItem(db.albums.get(i));
			comboListaEdytowanychAlbumow.addItem(db.albums.get(i));
		}
		
		JButton btnDodajUtwr = new JButton(db.translations.get(26));
		btnDodajUtwr.setBounds(160, 112, 130, 23);
		btnDodajUtwr.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				String nazwa=nazwaUtworu.getText();
				if(!StringUtils.isBlank(nazwa)) {
					
					comboListaUtworow.addItem(nazwa);
					
					utwory utwor=new utwory();
					utwor.setNazwaUtworu(nazwaUtworu.getText());
		
					String nazwaWybranegoAlbumu=comboListaAlbumow.getSelectedItem().toString();
					
					db.addNewSong(utwor.getNazwaUtworu(), nazwaWybranegoAlbumu);
				}				
			}
		});
		frmUtwory.getContentPane().add(btnDodajUtwr);
		
		comboListaUtworow.addActionListener(new ActionListener() {
			/* w tym listenerku wybranie jakiegos utworu z listy comboListaUtworow powoduje, ze w polu edycji
			(nazwaEdytowanegoUtworu i comboListaEdytowanychAlbumow) wyswietlaja sie parametry wybranego utworu,
			czyli jego nazwa i album */
			public void actionPerformed(ActionEvent e) {
				
				int id=comboListaUtworow.getSelectedIndex();
				
				String utworZListy=comboListaUtworow.getSelectedItem().toString();
				
				db.selectSongByName(utworZListy); //selectSongById(id)
				System.out.println(db.selectedSong.getNazwaAlbumu());
				
				comboListaEdytowanychAlbumow.setSelectedItem(db.selectedSong.getNazwaAlbumu());
				nazwaEdytowanegoUtworu.setText(db.selectedSong.getNazwaUtworu());

			}
		});
		
		
		
		JLabel lblNazwaUtworu = new JLabel(db.translations.get(24));
		lblNazwaUtworu.setBounds(111, 44, 131, 14);
		frmUtwory.getContentPane().add(lblNazwaUtworu);
		
		JLabel lblNazwaAlbumu = new JLabel(db.translations.get(25));
		lblNazwaAlbumu.setBounds(111, 69, 101, 14);
		frmUtwory.getContentPane().add(lblNazwaAlbumu);
		
		nazwaUtworu = new JTextField();
		nazwaUtworu.setBounds(240, 41, 86, 20);
		frmUtwory.getContentPane().add(nazwaUtworu);
		nazwaUtworu.setColumns(10);
		
		JLabel lblListaUtworw = new JLabel(db.translations.get(27));
		lblListaUtworw.setBounds(59, 193, 249, 14);
		frmUtwory.getContentPane().add(lblListaUtworw);		
		
		JButton btnKasujUtwr = new JButton(db.translations.get(28));
		btnKasujUtwr.setBounds(59, 311, 144, 23);
		btnKasujUtwr.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				
				String kasowanyUtwor=comboListaUtworow.getSelectedItem().toString();
				
				db.deleteSong(kasowanyUtwor);

				comboListaUtworow.removeItem(kasowanyUtwor);
				
				db.selectAllSongs();
				frmUtwory.setVisible(false);
				frmUtwory.setVisible(true);
			}
		});
		frmUtwory.getContentPane().add(btnKasujUtwr);
		
		JButton btnEdytujUtwr = new JButton(db.translations.get(29));
		btnEdytujUtwr.setBounds(258, 311, 144, 23);
		btnEdytujUtwr.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String edytowanyUtwor=comboListaUtworow.getSelectedItem().toString();
				String nazwaNowa = nazwaEdytowanegoUtworu.getText();
				String albumNowy = comboListaEdytowanychAlbumow.getSelectedItem().toString();
				System.out.println(albumNowy);
				
				if(!StringUtils.isBlank(nazwaNowa)) {
					
					db.updateSong(nazwaNowa, albumNowy, edytowanyUtwor);
					
					int n=comboListaUtworow.getSelectedIndex();
					comboListaUtworow.insertItemAt(nazwaNowa, n);
					comboListaUtworow.removeItem(edytowanyUtwor);
					
					frmUtwory.setVisible(false);
					frmUtwory.setVisible(true);
				}		
			}
		});
		frmUtwory.getContentPane().add(btnEdytujUtwr);
		
		JLabel lblDodawanieUtworu = new JLabel(db.translations.get(23));
		lblDodawanieUtworu.setBounds(168, 11, 140, 14);
		frmUtwory.getContentPane().add(lblDodawanieUtworu);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(55, 157, 347, 2);
		frmUtwory.getContentPane().add(separator);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(276, 11, 1, 158);
		frmUtwory.getContentPane().add(separator_1);
		
		JLabel label = new JLabel(db.translations.get(24));
		label.setBounds(111, 234, 131, 14);
		frmUtwory.getContentPane().add(label);
		
		JLabel label_1 = new JLabel(db.translations.get(25));
		label_1.setBounds(111, 265, 101, 14);
		frmUtwory.getContentPane().add(label_1);
		
	}
}