import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.JLabel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Sklep extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Sklep frame = new Sklep();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
 
	
	public Sklep() {
		
		
		Database db = new Database(); 
		db.getSelectedLanguage();
    	
	    if(db.lang == null) 
	    {
	    	db.lang = "pl";
	    }
	    System.out.println("Obecny jezyk to: " + db.lang);
		db.selectTranslationsByLanguage(db.lang);
		
		
		
		setTitle(db.translations.get(7));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 615, 418);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton Ustawienia = new JButton(db.translations.get(35));
		Ustawienia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Ustawienia oknoUst = new Ustawienia();
				oknoUst.setVisible(true);
			}
		});
		Ustawienia.setBounds(437, 11, 120, 23);
		contentPane.add(Ustawienia);
	
		JLabel lblWitamy = new JLabel(db.translations.get(34));
		lblWitamy.setBounds(206, 15, 169, 14);
		contentPane.add(lblWitamy);
		
		JLabel username = new JLabel(Logowanie.loggedUser);
		username.setBounds(275, 15, 46, 14);
		contentPane.add(username);
		
		
		
	
		db.selectAllAlbums();
		JList list = new JList(db.albums.toArray());
		list.setBounds(10, 88, 579, 182);
		contentPane.add(list);
		JButton Kup_album = new JButton(db.translations.get(33));
		Kup_album.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String userd=username.getText();			
			String album=list.getSelectedValue().toString();
			db.addNewOrder(Logowanie.loggedUser,album);
			}
		});
	
		Kup_album.setBounds(10, 11, 130, 23);
		contentPane.add(Kup_album);
	}
}
