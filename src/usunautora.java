import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTextField;

public class usunautora {

	private JFrame frame;
	private JLabel lblAutorzy;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					usunautora window = new usunautora();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public usunautora() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

        frame = new JFrame();
        frame.setBounds(100, 100, 730, 425);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        
	     Database db = new Database();
			db.getSelectedLanguage();
	    	
		    if(db.lang == null) 
		    {
		    	db.lang = "pl";
		    }
		    System.out.println("Obecny jezyk to: " + db.lang);
			db.selectTranslationsByLanguage(db.lang);
		
		db.selectAllAuthors();
		
		
		JScrollPane scrollableList = new JScrollPane();
		scrollableList.setLocation(212, 40);
		scrollableList.setSize(100, 100);
		
		frame.getContentPane().add(scrollableList);
		
		
		
		
		
		
		JList list3;
		list3 = new JList(db.authors.toArray());
		scrollableList.setViewportView(list3);
		list3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			
			}
		});
		
		list3.setBounds(184, 11, 89, 92);
		//frame.getContentPane().add(list3);
		//frame.getContentPane().add(list);
		list3.setVisibleRowCount(1);
		list3.setBackground(Color.WHITE);
		
		JButton btnUsu = new JButton(db.translations.get(12));
		btnUsu.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String idauto= (String) list3.getSelectedValue();
				db.deleteAuthor(idauto);
				
				
				frame.dispose();
				usunautora window = new usunautora();
				window.frame.setVisible(true);
			}
		});
		btnUsu.setBounds(222, 159, 89, 23);
		frame.getContentPane().add(btnUsu);
		
		lblAutorzy = new JLabel(db.translations.get(11));
		lblAutorzy.setBounds(85, 42, 46, 14);
		frame.getContentPane().add(lblAutorzy);
		
	}
}
