import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Search {

	public static JTable table;
	public static DefaultTableModel model;
	private JFrame frame;
	private JTextField textField;
	private JPanel contentPane;
	
	
	private JTextField txtPattern;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					Search window = new Search();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Search() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		Database db = new Database();
		db.getSelectedLanguage();
    	
	    if(db.lang == null) 
	    {
	    	db.lang = "pl";
	    }
	    System.out.println("Obecny jezyk to: " + db.lang);
		db.selectTranslationsByLanguage(db.lang);
		//db.addNewAlbum();
		
		frame = new JFrame();
		frame.setBounds(0, 0, 800, 600);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		

		

		
		Label label = new Label(db.translations.get(30));
		label.setFont(new Font("Malgun Gothic", Font.PLAIN, 18));
		label.setBounds(287, 36, 144, 22);
		frame.getContentPane().add(label);
		
		
		

		
		
		
		
		
		
		
		
		
		
		model = new DefaultTableModel(); 
		

		// Create a couple of columns
		model.addColumn(db.translations.get(31));
		model.addColumn(db.translations.get(32));
		model.addColumn(db.translations.get(17));
		model.addColumn(db.translations.get(11));

		// Append a row 
		//model.addRow(new Object[]{"1", "2", "3", "4"});
		
		table = new JTable(model);
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(20,  235,  658,  254);
		//table.setFillsViewportHeight(true);
		
		
		frame.getContentPane().add(scrollPane);
		
		txtPattern = new JTextField();
		
		txtPattern.setBounds(238, 64, 192, 38);
		frame.getContentPane().add(txtPattern);
		txtPattern.setColumns(10);
		txtPattern.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				model.setRowCount(0);
				db.selectSongsBySearch(txtPattern.getText());
				
			}
		});
		
	}
}
