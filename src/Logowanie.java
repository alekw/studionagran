import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang3.StringUtils;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.BigInteger;
import java.security.MessageDigest;

public class Logowanie extends JFrame {
	
	static String loggedUser;
	private JPanel contentPane;
	private JTextField imief;
	private JPasswordField haslof;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Logowanie frame = new Logowanie();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Logowanie() {
		Database db = new Database();
		db.getSelectedLanguage();
    	
	    if(db.lang == null) 
	    {
	    	db.lang = "pl";
	    }
	    System.out.println("Obecny jezyk to: " + db.lang);
		db.selectTranslationsByLanguage(db.lang);
		setTitle(db.translations.get(37));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 455, 278);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JList clientList;
		//clientList = new JList(db.client.toArray());
		
		
		
		
		
		
		JLabel Imie_log = new JLabel(db.translations.get(38));
		Imie_log.setBounds(40, 24, 86, 14);
		contentPane.add(Imie_log);
		
		imief = new JTextField();
		imief.setBounds(161, 21, 86, 20);
		contentPane.add(imief);
		imief.setColumns(10);
		
		JLabel Haslo_log = new JLabel(db.translations.get(39));
		Haslo_log.setBounds(40, 66, 86, 14);
		contentPane.add(Haslo_log);
		
		haslof = new JPasswordField();
		haslof.setBounds(161, 64, 86, 17);
		contentPane.add(haslof);
		
		JButton Zatwierdz_log = new JButton(db.translations.get(40));
		Zatwierdz_log.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String name=imief.getText();
				String passwordnieszyfrowane=haslof.getText();
				String passwordsha1 = null;
				String password = null;
				
			if(!StringUtils.isBlank(name) && !StringUtils.isBlank(passwordnieszyfrowane)) {
				try {
					MessageDigest digest = MessageDigest.getInstance("SHA-1");
			        digest.reset();
			        digest.update(passwordnieszyfrowane.getBytes("utf8"));
			        passwordsha1 = String.format("%040x", new BigInteger(1, digest.digest()));
				} catch (Exception e1){
					e1.printStackTrace();
				}
				
				try {
					MessageDigest digest1 = MessageDigest.getInstance("SHA-1");
			        digest1.reset();
			        digest1.update(passwordsha1.getBytes("utf8"));
			        password = String.format("%040x", new BigInteger(1, digest1.digest()));
				} catch (Exception e1){
					e1.printStackTrace();
				}
				
				
				
		
				if(db.checkUserPassword(name, password)) 
				{
					loggedUser = name;
					new Sklep().main(null);}			
				}
			}
		});
		Zatwierdz_log.setBounds(40, 133, 124, 23);
		contentPane.add(Zatwierdz_log);
	}
}

