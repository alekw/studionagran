import java.awt.EventQueue;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;



public class GUI {
	public static ArrayList klienci;
	private JFrame frmGui;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		klienci=new ArrayList();
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					 
					window.frmGui.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		Database db = new Database();
		db.getSelectedLanguage();
    	
	    if(db.lang == null) 
	    {
	    	db.lang = "pl";
	    }
	    System.out.println("Obecny jezyk to: " + db.lang);
		db.selectTranslationsByLanguage(db.lang);
		frmGui = new JFrame();
		frmGui.setTitle(db.translations.get(8));
		frmGui.setBounds(100, 100, 627, 423);
		frmGui.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmGui.getContentPane().setLayout(null);
		
		JButton Rejestracja = new JButton(db.translations.get(36));
		Rejestracja.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Rejestracja oknoRej=new Rejestracja();
				oknoRej.setVisible(true);
			}
		});
		Rejestracja.setBounds(10, 11, 152, 23);
		frmGui.getContentPane().add(Rejestracja);
		
		JButton Logowanie = new JButton(db.translations.get(37));
		Logowanie.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Logowanie oknoLog = new Logowanie();
				oknoLog.setVisible(true);
			}
		});
		Logowanie.setBounds(193, 11, 125, 23);
		frmGui.getContentPane().add(Logowanie);
	}
}
